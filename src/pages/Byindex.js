import React, { useState, useEffect } from "react";
import axios from "axios";
import Cards from "../components/Cards";
import NavbarTop from '../components/NavBar';
import Footer from "../components/Footer";
import Spinner from 'react-bootstrap/Spinner'



const api = axios.create({
  baseURL: "http://treinamentoajax.herokuapp.com/",
});

export default function Byindex(props) {
  const [msgdata, setMsgdata] = useState([]);
  const[orderBy, setOrderBy] = useState(null);



  const orderByType = (type) => {

    const orderByMessage = () => {
      const dataByMessage = msgdata.sort(function(a, b){return a.message - b.message}); 
      console.log(dataByMessage)
      setOrderBy("Message")
      setMsgdata(dataByMessage);
    };

    const orderByAuthor = () => {
      const dataByAuthor = msgdata.sort(function(a, b){return a.name - b.name}); 
      setOrderBy("Author")
      setMsgdata(dataByAuthor);
    };

    const orderByNewer = () => {
      const dataByNewer = msgdata.sort(function(a, b){return b.id - a.id}); 
      setOrderBy("Newer")
      setMsgdata(dataByNewer);
    };

    const orderByOlder = () => {
      const dataByOlder = msgdata.sort(function(a, b){return a.id - b.id}); 
      setOrderBy("Older")
      setMsgdata(dataByOlder);
    };


    console.log(type);
    switch (type) {
      case "newer":
        orderByNewer();
        console.log("caso1")
        break;
        
      case "older":
        orderByOlder();
        console.log("caso2")
        break;
      case "author":
        orderByAuthor();
        console.log("caso3")
        break;
      case "msg":
        console.log("caso4")
        orderByMessage();
        break;
      
      default:
        break;
    }
  };

  

  useEffect(() => {
    console.log("Effect switch")
  }, [orderBy])




  
  useEffect( () => {
    const fetchData = async () => {
      const response = await api.get("messages");
      const data = response.data; 
      //ordenando de forma cronológica como padrão/inicial dos dados
      data.sort(function(a, b){return b.id - a.id});
      //desestruturando o array inicial e criando campos adicionais 
      //lowercase para os campos do tipo string e convertendo dados
      // timestamp para mes e dia objetivando possibilitar as funcionalidades
      // de ordenar e agrupar dados aplicadas via navbar


      const formattedData = data.map( ({id, name, message, created_at, updated_at}) => {
        return {
          id,
          name,
          filtername: name.toLowerCase().trim(),
          message,
          filtermessage: message.toLowerCase(),
          created_at,
          formatCreated_atDate: new Date(created_at).toLocaleString("pt-BR"),
          filterCreated_atDate: created_at,
          updated_at,
          formatUpdated_at: new Date(updated_at).toLocaleString("pt-BR"),
        };
      });
      console.log(formattedData);
      
      console.log("Effect - Load")
      setTimeout(() => {
        setMsgdata(formattedData);
      }, 1000);
    };
    fetchData();
  }, []);

 

  return (
    <div className="default-flex-column">
      
      <NavbarTop orderByType={orderByType}/>
      {msgdata.length ? <Cards msglist={msgdata} /> : <Spinner id="spinner" animation="border" variant="dark" /> }
      <Footer />
      
    </div>
  );
}

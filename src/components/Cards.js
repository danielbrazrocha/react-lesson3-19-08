import React from "react";
import Card from "./Card";

export default function Cards({msglist}) {
   
  return (
    <div className="cards">
      <ul>
        {msglist.map((item) => {
          return (
            <Card key={item.id} msg={item} />
          );
        })}
      </ul>
    </div>
  );
}

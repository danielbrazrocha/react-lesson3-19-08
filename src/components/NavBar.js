import React from "react";
import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";
import NavDropdown from "react-bootstrap/NavDropdown";

export default function NavBar({orderByType}) {

  const handleOrderBy = (event) => {
    orderByType(event.target.id);
    console.log("passagem por switch")
  };
  


  return (
    <Nav id="NavBarBg" fixed="top" className="bg-dark">
      <Navbar id="testBar" className="bg-dark" variant="dark" fixed="bottom">
        <Navbar
          collapseOnSelect
          expand="lg"
          bg="dark"
          variant="dark"
          fixed="top"
        >
          <Nav id="NavBarTop">
            <Navbar.Brand href="#home">
              <img
                alt="IN Junior Logo"
                src="https://injunior.com.br/wp-content/uploads/2018/11/logo-branca.png"
                width="30"
                height="30"
                className="d-inline-block align-top"
              />{" "}
            </Navbar.Brand>
            <Navbar.Toggle aria-controls="responsive-navbar-nav" />
            <Navbar.Collapse id="responsive-navbar-nav">
              <Nav className="mr-auto">
                <Navbar.Brand className="headerh1" href="#home">
                  IN Junior - Message Center
                </Navbar.Brand>
              </Nav>
              <Nav>
                <NavDropdown title="Ordenar" id="basic-nav-dropdown">
                  <NavDropdown.Item id="newer" onClick={handleOrderBy}>Mais Novo</NavDropdown.Item>
                  <NavDropdown.Item id="older" onClick={handleOrderBy}>Mais Antigo</NavDropdown.Item>
                  <NavDropdown.Item id="author" onClick={handleOrderBy}>Autor</NavDropdown.Item>
                  <NavDropdown.Item id="msg" onClick={handleOrderBy}>Mensagem</NavDropdown.Item>
                </NavDropdown>

                <NavDropdown title="Agrupar" id="basic-nav-dropdown">
                  <NavDropdown.Item href="#action/3.1">
                    Autor
                  </NavDropdown.Item>
                  <NavDropdown.Item href="#action/3.2">
                    Dia
                  </NavDropdown.Item>
                  <NavDropdown.Item href="#action/3.3">Atualizadas hoje</NavDropdown.Item>
                </NavDropdown>

                <Nav.Link href="#deets">Nova Mensagem</Nav.Link>
              </Nav>
            </Navbar.Collapse>
          </Nav>
        </Navbar>
      </Navbar>
    </Nav>
  );
}

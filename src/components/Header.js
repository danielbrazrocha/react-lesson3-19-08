import React from 'react'
import Button from 'react-bootstrap/Button';

export default function Header() {
    return (
        <div className="header">
            <img src="https://injunior.com.br/wp-content/uploads/2018/11/logo-branca.png" alt="IN Junior Logo"></img>
            <h1> IN Junior - Message Center</h1>
            <Button href="#">Link</Button> <Button type="submit">Button</Button>{' '}
        </div>
    )
}

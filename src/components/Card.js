import React from "react";
import { calcHoursDiff } from "../helpers/DateFormat";
import "./Card.css";
import "../img/profile.png";

export default function Card({ msg }) {
  return (
    <div className="card padding">
      <li key={msg.id}>
        <div id="card_content">
          <div id="card_content_left">
            <div>
              <img src="https://pondokindahmall.co.id/assets/img/default.png"></img>
            </div>
            <div id="card_footer">
              <p><span id="msg_number">{msg.id}</span></p>
            </div>
          </div>

          <div id="card_content_right">
            <div id="card_header">
              <div id="card_msg">
                <p>
                  <span id="author_name">{msg.name}</span>
                </p>
              </div>

              <div id="card_time">
                <p>{" "} · {calcHoursDiff(msg.created_at)}h atrás</p>
              </div>
            </div>
            <div>
              <p><span>msg {msg.message}</span></p>
            </div>
          </div>
        </div>
      </li>
    </div>
  );
}
